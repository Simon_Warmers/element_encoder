periodic_table_list = [
("H","Hydrogen","Wasserstoff"),
("He","Helium","Helium"),
("Li","Lithium","Lithium"),
("Be","Beryllium","Beryllium"),
("B","Boron","Bor"),
("C","Carbon","Kohlenstoff"),
("N","Nitrogen","Stickstoff"),
("O","Oxygen","Sauerstoff"),
("F","Fluorine","Fluor"),
("Ne","Neon","Neon"),
("Na","Sodium","Natrium"),
("Mg","Magnesium","Magnesium"),
("Al","Aluminum","Aluminium"),
("Si","Silicon","Silicium"),
("P","Phosphorus","Phosphor"),
("S","Sulfur","Schwefel"),
("Cl","Chlorine","Chlor"),
("Ar","Argon","Argon"),
("K","Potassium","Kalium"),
("Ca","Calcium","Calcium"),
("Sc","Scandium","Scandium"),
("Ti","Titanium","Titan"),
("V","Vanadium","Vanadium"),
("Cr","Chromium","Chrom"),
("Mn","Manganese","Mangan"),
("Fe","Iron","Eisen"),
("Co","Cobalt","Cobalt"),
("Ni","Nickel","Nickel"),
("Cu","Copper","Kupfer"),
("Zn","Zinc","Zink"),
("Ga","Gallium","Gallium"),
("Ge","Germanium","Germanium"),
("As","Arsenic","Arsen"),
("Se","Selenium","Selen"),
("Br","Bromine","Brom"),
("Kr","Krypton","Krypton"),
("Rb","Rubidium","Rubidium"),
("Sr","Strontium","Strontium"),
("Y","Yttrium","Yttrium"),
("Zr","Zirconium","Zirconium"),
("Nb","Niobium","Niob"),
("Mo","Molybdenum","Molibdän"),
("Tc","Technetium","Technetium"),
("Ru","Ruthenium","Ruthenium"),
("Rh","Rhodium","Rhodium"),
("Pd","Palladium","Palladium"),
("Ag","Silver","Silber"),
("Cd","Cadmium","Cadmium"),
("In","Indium","Indium"),
("Sn","Tin","Zinn"),
("Sb","Antimony","Antimon"),
("Te","Tellurium","Tellur"),
("I","Iodine","Iod"),
("Xe","Xenon","Xenon"),
("Cs","Cesium","Cäsium"),
("Ba","Barium","Barium"),
("La","Lanthanum","Lanthan"),
("Ce","Cerium","Cer"),
("Pr","Praseodymium","Praseodym"),
("Nd","Neodymium","Neodym"),
("Pm","Promethium","Promethium"),
("Sm","Samarium","Samarium"),
("Eu","Europium","Europium"),
("Gd","Gadolinium","Gadolinium"),
("Tb","Terbium","Terbium"),
("Dy","Dysprosium","Dysprosium"),
("Ho","Holmium","Holmium"),
("Er","Erbium","Erbium"),
("Tm","Thulium","Thulium"),
("Yb","Ytterbium","Ytterbium"),
("Lu","Lutetium","Lutetium"),
("Hf","Hafnium","Hafnium"),
("Ta","Tantalum","Tantal"),
("W","Tungsten","Wolfram"),
("Re","Rhenium","Rhenium"),
("Os","Osmium","Osmium"),
("Ir","Iridium","Iridium"),
("Pt","Platinum","Platin"),
("Au","Gold","Gold"),
("Hg","Mercury","Quecksilber"),
("Tl","Thallium","Thallium"),
("Pb","Lead","Blei"),
("Bi","Bismuth","Bismut"),
("Po","Polonium","Polonium"),
("At","Astatine","Astat"),
("Rn","Radon","Radon"),
("Fr","Francium","Francium"),
("Ra","Radium","Radium"),
("Ac","Actinium","Actinium"),
("Th","Thorium","Thorium"),
("Pa","Protactinium","Protactinium"),
("U","Uranium","Uran"),
("Np","Neptunium","Neptunium"),
("Pu","Plutonium","Plutonium"),
("Am","Americium","Americium"),
("Cm","Curium","Curium"),
("Bk","Berkelium","Berkelium"),
("Cf","Californium","Californium"),
("Es","Einsteinium","Einsteinium"),
("Fm","Fermium","Fermium"),
("Md","Mendelevium","Mendelevium"),
("No","Nobelium","Nobelium"),
("Lr","Lawrencium","Lawrencium"),
("Rf","Rutherfordium","Rutherfordium"),
("Db","Dubnium","Dubnium"),
("Sg","Seaborgium","Seaborgium"),
("Bh","Bohrium","Bohrium"),
("Hs","Hassium","Hassium"),
("Mt","Meitnerium","Meitnerium"),
("Ds","Darmstadtium","Darmstadtium"),
("Rg","Roentgenium","Roentgenium"),
("Cn","Copernicium","Copernicium"),
("Nh","Nihonium","Nihonium"),
("Fl","Flerovium","Flerovium"),
("Mc","Moscovium","Moscovium"),
("Lv","Livermorium","Livermorium"),
("Ts","Tennessine","Tenness"),
("Og","Oganesson","Oganesson"),
]

def search_element(s, output, exit):
    try:
        s2, exit_2 = search_element_2(s, output, exit)
    except:
        return output

    try:
        s1, exit_1 = search_element_1(s, output, exit)
    except:
        return output



    if exit_1 or exit_2 or exit:
        return output


def search_element_1(s, output, exit):
    #print("S1",s,  output)
    if len(s) < 1 or exit:
        return output, True
    tmp_str = s[:1]
    tmp_element = find_element(tmp_str)
    if tmp_element:
        output.append(tmp_element)
        search_element(s[1:], output, exit)
    else:
        return output, True
def search_element_2(s, output, exit):
    #print("S2", s,  output)
    if len(s) < 2 or exit:
        return output, True
    tmp_str = s[:2]
    tmp_element = find_element(tmp_str)
    if tmp_element:
        output.append(tmp_element)
        search_element(s[2:], output, exit)
    else:
        return output, True
def find_element(s):
    return [item for item in periodic_table_list if item[0].upper() == s]

def print_list(list):
    return str(list).replace("[", "").replace("]", "").replace("\'", "").replace(",", "").replace(";", ",").replace("-", "\033[91m").replace("+", "\033[0m")

if __name__ == '__main__':
    # config
    REPLACE_MISSING_LETTERS = False
    REPLACE_MISSING_ELEMENTS = True


    name_list = input("Enter your Name:  ")
    print(f"Searching for \"{name_list}\" written in Elements\n\n")
    result_list_long = []
    result_list_long_ger = []
    result_list_short = []
    missing_buffer = []

    for name in name_list.split(" "):

        if name in ["?", ".", "!", ":"]:
            result_list_short.append(name)
            result_list_long.append(name)
            result_list_long_ger.append(name)
            continue

        output = []
        search_element(name.upper(), output, False)

        result_short = str([i[0][0] for i in output]).replace("[","").replace("]","").replace("\'","").replace(",","").replace(" ","")
        result_long = str([i[0][1] for i in output]).replace("[","").replace("]","").replace("\'","").replace(",","")
        result_long_ger = str([i[0][2] for i in output]).replace("[", "").replace("]", "").replace("\'", "").replace(",", "")

        if result_short.upper() != name.upper():
            name_old = name
            last_result_short = result_short

            while (True):
                diff = len(name_old) - len(result_short)
                if diff < 1:
                    break

                if len(name) != 0:
                    missing_letter = name.upper()[(len(last_result_short)):][0]

                    missing_letter_en = " ###"
                    missing_letter_ger = " ###"

                    if missing_letter == "Q":
                        missing_letter_en = "Quarkium".upper()

                    if missing_letter == "J":
                        missing_letter_en = "Joliotium".upper()
                        missing_letter_ger = "Joliotium".upper()

                    #find missing letter in the starting letters of the periodic table
                    for element in periodic_table_list:
                        if element[1][0] == missing_letter:
                            missing_letter_en = element[1].upper()
                            break


                    for element in periodic_table_list:
                        if element[2][0] == missing_letter:
                            missing_letter_ger = element[2].upper()
                            break




                    #print("missing_letter", missing_letter)
                    name = name.upper()[(len(last_result_short)+1):]

                result_short += "#"
                missing_buffer.append(missing_letter)

                if REPLACE_MISSING_ELEMENTS:
                    result_long += " " + missing_letter_en
                    result_long_ger += " " + missing_letter_ger
                else:
                    result_long += " ###"
                    result_long_ger += " ###"

                output = []
                search_element(name.upper(), output, False)

                last_result_short = str([i[0][0] for i in output]).replace("[", "").replace("]", "").replace("\'", "").replace(",", "").replace(" ", "")
                result_short += last_result_short
                result_long += " " + str([i[0][1] for i in output]).replace("[", "").replace("]", "").replace("\'", "").replace(",", "")
                result_long_ger += " " + str([i[0][2] for i in output]).replace("[", "").replace("]", "").replace("\'", "").replace(",", "")


        for letter in result_short:
            if letter == "#":
                if REPLACE_MISSING_LETTERS:
                    missing_buffer[0] = "-" + missing_buffer[0] + "+"
                else:
                    missing_buffer[0] = "-" + "*" + "+"
                result_short = result_short.replace(letter, missing_buffer[0], 1)
                missing_buffer.pop(0)

        result_list_short.append(result_short)

        result_list_long.append(result_long)
        result_list_long.append(";")

        result_list_long_ger.append(result_long_ger)
        result_list_long_ger.append(";")
       
    print(print_list(result_list_short), "\n")
    print(print_list(result_list_long), "\n")
    print(print_list(result_list_long_ger))







